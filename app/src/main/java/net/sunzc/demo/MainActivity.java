package net.sunzc.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import net.sunzc.demo.ui.activity.CommonLabActivity;
import net.sunzc.demo.ui.activity.DatabaseDemoActivity;
import net.sunzc.demo.ui.activity.ThreadPoolDemoActivity;
import net.sunzc.demo.ui.activity.ViewpagerDemoActivity;
import net.sunzc.demo.ui.adapter.IndexAdapter;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements IndexAdapter.OnIndexItemListener {

    private HashMap<String, Class> indexData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView demoIndex = (RecyclerView) findViewById(R.id.list_view);
        demoIndex.setHasFixedSize(true);
        demoIndex.setItemAnimator(demoIndex.getItemAnimator());
        demoIndex.setLayoutManager(new LinearLayoutManager(this));//线性展示
//        List<HashMap<String, Class>> indexData = new ArrayList<HashMap<String, Class>>() {{
//            add(new HashMap<String, Class>() {{
//                put("滑动切换页面Demo", ViewpagerDemoActivity.class);
//            }});
//            add(new HashMap<String, Class>() {{
//                put("线程池Demo", ThreadPoolDemoActivity.class);
//            }});
//
//
//        }};
        indexData = new HashMap<String, Class>() {{
            put("滑动切换页面Demo", ViewpagerDemoActivity.class);
            put("线程池Demo", ThreadPoolDemoActivity.class);
            put("数据库使用Demo", DatabaseDemoActivity.class);
            put("实验桌Demo", CommonLabActivity.class);
        }};
        IndexAdapter indexAdapter = new IndexAdapter(this, indexData);
        demoIndex.setAdapter(indexAdapter);
        indexAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(TextView mTextView, String key) {
        startActivity(new Intent(this, indexData.get(key)));
    }
}
