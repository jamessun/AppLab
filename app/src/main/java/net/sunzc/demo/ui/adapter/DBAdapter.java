package net.sunzc.demo.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import net.sunzc.demo.R;
import net.sunzc.demo.model.TestEntity;
import net.sunzc.demo.ui.widget.DBItems;

import java.util.ArrayList;

/**
 * Created by 振朝 on 2015/9/20.
 */
public class DBAdapter extends BaseAdapter {
    private final Context mContext;
    private final ArrayList<TestEntity> testEntities;

    public DBAdapter(Context context, ArrayList<TestEntity> dataList) {
        mContext = context;
        testEntities = dataList;
    }

    @Override
    public int getCount() {
        return testEntities.size();
    }

    @Override
    public TestEntity getItem(int position) {
        return testEntities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void notifyDataSetChanged(ArrayList<TestEntity> entities) {
        testEntities.clear();
        testEntities.addAll(entities);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DBItems itemsView;
        if (convertView == null)
            itemsView = (DBItems) LayoutInflater.from(mContext).inflate(R.layout.db_items, null);
        else
            itemsView = (DBItems) convertView;
        TestEntity item = getItem(position);
        itemsView.showText(item);
        return itemsView;
    }
}
