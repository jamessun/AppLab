package net.sunzc.demo.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.RadioGroup;

import net.sunzc.demo.R;
import net.sunzc.demo.listener.OnFragmentInteractionListener;
import net.sunzc.demo.ui.fragment.FirstFragment;
import net.sunzc.demo.ui.fragment.SecondFragment;

import java.util.ArrayList;
import java.util.Collections;

public class ViewpagerDemoActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    private FragmentManager fragmentMangager;
    private SecondFragment fragment21, fragment22, fragment23;
    private FirstFragment fragment11, fragment12, fragment13;
    private RadioGroup radioGroup, radioGroup1;
    private ViewPager viewPager;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager_demo);
        radioGroup = (RadioGroup) findViewById(R.id.rg);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb01:
                        radioGroup1.check(R.id.rb1);
                        break;
                    case R.id.rb02:
                        radioGroup1.check(R.id.rb4);
                        break;
                }
            }
        });
        radioGroup1 = (RadioGroup) findViewById(R.id.rg1);
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int itemPosition = viewPager.getCurrentItem();
                switch (checkedId) {
                    case R.id.rb1:
                        viewPager.setCurrentItem(0);
                        fragment11.show("第一组第一个");
                        break;
                    case R.id.rb2:
                        viewPager.setCurrentItem(1);
                        fragment12.show("第一组第二个");
                        break;
                    case R.id.rb3:
                        viewPager.setCurrentItem(2);
                        fragment13.show("第一组第三个");
                        break;
                    case R.id.rb4:
                        viewPager.setCurrentItem(3);
                        fragment21.show("第二组第一个");
                        break;
                    case R.id.rb5:
                        viewPager.setCurrentItem(4);
                        fragment22.show("第二组第二个");
                        break;
                    case R.id.rb6:
                        viewPager.setCurrentItem(5);
                        fragment23.show("第二组第三个");
                        break;
                }
            }
        });
        fragmentMangager = getSupportFragmentManager();
        initViewPager();
        Log.i("123456", "Activity create" + System.currentTimeMillis());
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.frag_container);
        fragment11 = FirstFragment.newInstance("1", null);
        fragment12 = FirstFragment.newInstance(null, null);
        fragment13 = FirstFragment.newInstance(null, null);
        fragment21 = SecondFragment.newInstance(null, null);
        fragment22 = SecondFragment.newInstance(null, null);
        fragment23 = SecondFragment.newInstance(null, null);
        ViewPagerAdapter adapter = new ViewPagerAdapter(fragmentMangager);
        adapter.setFragments(new Fragment[]{fragment11, fragment12, fragment13, fragment21, fragment22, fragment23});
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                position = i;
                Log.i("PageSelected", i + "当前页面为");
                switch (i) {
                    case 0:
                        radioGroup.check(R.id.rb01);
                        radioGroup1.check(R.id.rb1);
                        break;
                    case 1:
                        radioGroup.check(R.id.rb01);
                        radioGroup1.check(R.id.rb2);
                        break;
                    case 2:
                        radioGroup.check(R.id.rb01);
                        radioGroup1.check(R.id.rb3);
                        break;
                    case 3:
                        radioGroup.check(R.id.rb02);
                        radioGroup1.check(R.id.rb4);
                        break;
                    case 4:
                        radioGroup.check(R.id.rb02);
                        radioGroup1.check(R.id.rb5);
                        break;
                    case 5:
                        radioGroup.check(R.id.rb02);
                        radioGroup1.check(R.id.rb6);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                Log.i("PageSelected", "onPageScrollStateChanged=" + i);
                if (i == 0) show();
            }
        });
    }

    public void show() {
        Log.i("PageSelected", "执行show方法，最后的页面为：" + position);
    }

    private void setCurrentPage(int i) {


    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {


        private ArrayList<Fragment> mFragments = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        public void setFragments(Fragment[] fragments) {
            Collections.addAll(mFragments, fragments);
        }

        public void setFragments(ArrayList<Fragment> fragments) {
            mFragments = fragments;
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

    @Override
    public void onFragmentInteraction(int label) {
        switch (label) {
            case 1:
                radioGroup.check(R.id.rb01);
                break;
            case 2:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("ViewpagerDemoActivity", "ViewpagerDemoActivity,onActivityResult");
    }
}
