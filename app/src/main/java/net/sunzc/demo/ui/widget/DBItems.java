package net.sunzc.demo.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ta.common.TAStringUtils;

import net.sunzc.demo.R;
import net.sunzc.demo.model.TestEntity;

/**
 * Created by 振朝 on 2015/9/19.
 */
public class DBItems extends LinearLayout {
    private TextView tvA, tvB, tvSum;

    public DBItems(Context context) {
        super(context);
    }

    public DBItems(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DBItems(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    private void initView() {
        tvA = (TextView) findViewById(R.id.tvA);
        tvB = (TextView) findViewById(R.id.tvB);
        tvSum = (TextView) findViewById(R.id.tvSum);
    }


    public void showText(TestEntity item) {
        if (item == null) item = new TestEntity(0, 0, 0);
        String a = item.getA() + "";
        String b = item.getB() + "";
        String sum = item.getSum() + "";
        if (!TAStringUtils.isBlank(a)) tvA.setText(a);
        if (!TAStringUtils.isBlank(b)) tvB.setText(b);
        if (!TAStringUtils.isBlank(sum)) tvSum.setText(sum);
    }
}
