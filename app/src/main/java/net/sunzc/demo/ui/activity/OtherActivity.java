package net.sunzc.demo.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.sunzc.demo.R;

import java.util.ArrayList;
import java.util.HashMap;

public class OtherActivity extends AppCompatActivity {
    HashMap<String, Object> dataMap, dataMap3;
    SimpleAdapter adapter;
    private TextView tv;
    private EditText et;
    private ListView lv;
    String[] from = new String[]{"tv1", "tv2"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);
        Bundle receiveIntent = getIntent().getExtras();
        tv = (TextView) findViewById(R.id.tv);
        tv.setText(receiveIntent.getString("jump"));
        et = (EditText) findViewById(R.id.et);
        lv = (ListView) findViewById(R.id.list_view);
        int i = 0, m = 100;
        ArrayList<HashMap<String, Object>> data = new ArrayList<>();
        ArrayList<Integer> list = new ArrayList<>();
        HashMap<String, Integer> map = new HashMap<>();

        //测试在map或者list中添加map或者list时，只需改变子map或者子list的内容就添加是否可行
        //结果不可行，仅仅清楚子map，list中的数据，然后再添加，母map或者list中的子数据也会跟着修改
        //从中可以测出map，list中添加数据的实质是添加堆内存中的对象，而不是一个引用；
        for (int l = 0; l < 10; l++) {
            if (l == 3) {
                dataMap3 = new HashMap<>();
                dataMap3.put(from[0], i++);
                dataMap3.put(from[1], m++);
                data.add(l, dataMap3);
            } else {
                dataMap = new HashMap<>();
                dataMap.put(from[0], i++);
                dataMap.put(from[1], m++);
                data.add(l, dataMap);
            }
        }
        adapter = new SimpleAdapter(this, data, R.layout.listview_item, from, new int[]{R.id.lv_tv1, R.id.lv_tv2});
        lv.setAdapter(adapter);
    }

    public void modify(View view) {
        dataMap.put(from[0], "哈哈");
        dataMap.put(from[1], "我修改了");
        adapter.notifyDataSetChanged();
    }


    public void send(View view) {
        String content = tv.getText().toString() + "//" + et.getText().toString();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("jump", content);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
