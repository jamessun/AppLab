package net.sunzc.demo.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import net.sunzc.demo.R;

/**
 * 这是个自定义View的事例类
 */
public class MyCustomView extends View {
    private String mExampleString; // TODO: 这里可以使用R.String来设置默认值
    private int mExampleColor = Color.RED; // TODO: 使用R.color来设置默认值
    private float mExampleDimension = 0; // TODO: 使用R.dimen来设置默认值
    private Drawable mExampleDrawable;

    private TextPaint mTextPaint;
    private float mTextWidth;
    private float mTextHeight;

    public MyCustomView(Context context) {
        super(context);
        init(null, 0);
    }

    public MyCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public MyCustomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // 载入系统属性和自定义属性
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.MyCustomView, defStyle, 0);

        mExampleString = a.getString(
                R.styleable.MyCustomView_exampleString);
        mExampleColor = a.getColor(
                R.styleable.MyCustomView_exampleColor,
                mExampleColor);
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        mExampleDimension = a.getDimension(
                R.styleable.MyCustomView_exampleDimension,
                mExampleDimension);

        if (a.hasValue(R.styleable.MyCustomView_exampleDrawable)) {
            mExampleDrawable = a.getDrawable(
                    R.styleable.MyCustomView_exampleDrawable);
            if (mExampleDrawable != null) {
                mExampleDrawable.setCallback(this);
            }
        }

        a.recycle();//回收属性

        // 设置一个默认的文字画板
        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        //更新从属性中获得的字体属性
        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
        mTextPaint.setTextSize(mExampleDimension);
        mTextPaint.setColor(mExampleColor);
        mTextWidth = mTextPaint.measureText(mExampleString);

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTextHeight = fontMetrics.bottom;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //注意把它们作为成员变量存储可以降低每次draw的开销
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        // 绘制文字
        canvas.drawText(mExampleString,
                paddingLeft + (contentWidth - mTextWidth) / 2,
                paddingTop + (contentHeight + mTextHeight) / 2,
                mTextPaint);

        //绘制出文字顶部的Drawable图片
        if (mExampleDrawable != null) {
            mExampleDrawable.setBounds(paddingLeft, paddingTop,
                    paddingLeft + contentWidth, paddingTop + contentHeight);
            mExampleDrawable.draw(canvas);
        }
    }

    /**
     * 得到例子中自定义属性中设置的字符串
     *
     * @return 属性中设置的字符串值
     */
    public String getExampleString() {
        return mExampleString;
    }

    /**
     * 设置例子中的字符串，在这个例子中，这个字符串就是需要绘制的文字
     *
     * @param exampleString 需要绘制的文字
     */
    public void setExampleString(String exampleString) {
        mExampleString = exampleString;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * 获取例子中设置的自定义属性中的颜色值
     *
     * @return 例子中的颜色值
     */
    public int getExampleColor() {
        return mExampleColor;
    }

    /**
     * 设置例子中的字体颜色属性值
     *
     * @param exampleColor 颜色属性值
     */
    public void setExampleColor(int exampleColor) {
        mExampleColor = exampleColor;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example dimension attribute value.
     *
     * @return The example dimension attribute value.
     */
    public float getExampleDimension() {
        return mExampleDimension;
    }

    /**
     * Sets the view's example dimension attribute value. In the example view, this dimension
     * is the font size.
     *
     * @param exampleDimension The example dimension attribute value to use.
     */
    public void setExampleDimension(float exampleDimension) {
        mExampleDimension = exampleDimension;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example drawable attribute value.
     *
     * @return The example drawable attribute value.
     */
    public Drawable getExampleDrawable() {
        return mExampleDrawable;
    }

    /**
     * Sets the view's example drawable attribute value. In the example view, this drawable is
     * drawn above the text.
     *
     * @param exampleDrawable The example drawable attribute value to use.
     */
    public void setExampleDrawable(Drawable exampleDrawable) {
        mExampleDrawable = exampleDrawable;
    }
}
