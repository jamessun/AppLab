package net.sunzc.demo.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ta.TAApplication;
import com.ta.util.db.TASQLiteDatabasePool;

import net.sunzc.demo.R;
import net.sunzc.demo.listener.ITag;


public class ThreadPoolDemoActivity extends AppCompatActivity implements ITag {

    private TASQLiteDatabasePool mDBPool;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_pool_demo);
        initDB();
    }

    private void initDB() {
        mDBPool = ((TAApplication) getApplication()).getSQLiteDatabasePool();
    }

    public void addDB(View view) {
        if (mDBPool.getSQLiteDatabase() != null) {
            Log.i(DB_TAG, "数据库连接池容量为" + mDBPool.getMaxSQLiteDatabase() + ";已经使用了" + count + "个");
            Toast.makeText(this, "创建第" + ++count + "个数据库", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "创建数据库失败", Toast.LENGTH_SHORT).show();
        }
    }
}
