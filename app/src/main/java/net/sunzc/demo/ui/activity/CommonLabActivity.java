package net.sunzc.demo.ui.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import net.sunzc.demo.R;

import java.util.LinkedHashMap;

public class CommonLabActivity extends AppCompatActivity {
    private static final int CONTINUE = 1;
    private static final int BREAK = 0;
    private static final int IMAGE = -1;
    private static final int TEXT = -2;

    TextView textView;
    ImageView imageView;
    private Handler workHandler;
    private Runnable mRunnable;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_lab);
        initView();
//        mapTest();
        HandlerThreadDemoTest();
    }

    private void initView() {
        textView = (TextView) findViewById(R.id.tv);
        imageView = (ImageView) findViewById(R.id.imageView);
    }

    /**
     * 显示图片
     *
     * @param view
     */
    public void showImage(View view) {
        mRunnable = new ImageRunnable();
        workHandler.post(mRunnable);
    }

    /**
     * 显示文字
     *
     * @param view
     */
    public void showText(View view) {
        mRunnable = new TextRunnable();
        workHandler.post(mRunnable);
    }

    private void mapTest() {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>() {{
            put("小名", "二蛋");
            put("大名", "蛋蛋");
        }};
        TextView tv = (TextView) findViewById(R.id.tv);
        String text = map.get("msg");
        tv.setText(text);
    }

    private void HandlerThreadDemoTest() {
        Log.d("ThreadDemoTest", "UI thread id=" + Thread.currentThread().getId());
        WorkHandlerThread workThread = new WorkHandlerThread("handlerThread1");
        workThread.start();
        workHandler = new Handler(workThread.getLooper(), workThread);
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                //在这里主线程可以接收子线程发送的消息，也可以向子线程发送消息
                super.handleMessage(msg);
                Log.d("ThreadDemoTest", "mMainHandler---thread id----=" + Thread.currentThread().getId());
                switch (msg.what) {
                    case IMAGE:
                        imageView.setImageDrawable((Drawable) msg.obj);
                        break;
                    case TEXT:
                        textView.setText(msg.obj.toString());
                        break;
                }
            }
        };

    }

    class ImageRunnable implements Runnable {

        @Override
        public void run() {
            Log.d("ThreadDemoTest", "ImageRunnable thread id----=" + Thread.currentThread().getId());
            Message msg = new Message();
            msg.what = CONTINUE;
            msg.arg1 = IMAGE;
            msg.obj = getResources().getDrawable(R.mipmap.ic_launcher);
            workHandler.sendMessage(msg);
        }
    }

    class TextRunnable implements Runnable {

        @Override
        public void run() {
            Log.d("ThreadDemoTest", "TextRunnable thread id----=" + Thread.currentThread().getId());
            Message msg = new Message();
            msg.what = CONTINUE;
            msg.arg1 = TEXT;
            msg.obj = "我已经出来啦！";
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            workHandler.sendMessage(msg);
        }
    }

    class WorkHandlerThread extends HandlerThread implements Handler.Callback {
        WorkHandlerThread(String name) {
            super(name);

        }

        @Override
        public boolean handleMessage(Message msg) {
            //在这里子线程可以想主线程发送消息
            Log.d("ThreadDemoTest", "工作线程的 id=" + Thread.currentThread().getId());
            Message mainMsg = new Message();
            switch (msg.what) {
                case CONTINUE:
                    mainMsg.what = msg.arg1;
                    mainMsg.obj = msg.obj;
                    mHandler.sendMessage(mainMsg);
                    return false;
                case BREAK:
                    return true;
                default:
                    return false;
            }
        }
    }
}
