package net.sunzc.demo.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.sunzc.demo.R;

import java.util.HashMap;

/**
 * 首页跳转的数据适配器
 * Created by 振朝 on 2015/9/17.
 */
public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.IndexViewHolder> {
    private final Context mContext;
    private final Object[] indexName;
    private OnIndexItemListener mOnItemClickListener;

    public IndexAdapter(Context context, HashMap<String, Class> indexData) {
        mContext = context;
        indexName = indexData.keySet().toArray();
    }

    @Override
    public IndexViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new IndexViewHolder(LayoutInflater.from(mContext).inflate(R.layout.index_item, null));
    }

    @Override
    public void onBindViewHolder(final IndexViewHolder holder, final int position) {
        holder.mTextView.setText(indexName[position].toString());
        holder.mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(holder.mTextView, indexName[position].toString());
            }
        });
    }

    /**
     * 设置条目点击的事件监听器
     *
     * @param mOnItemClickListener
     */
    public void setOnItemClickListener(OnIndexItemListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @Override
    public int getItemCount() {
        return indexName.length;
    }

    public interface OnIndexItemListener {

        void onItemClick(TextView mTextView, String key);
    }

    class IndexViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;

        public IndexViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.item);
        }
    }
}
