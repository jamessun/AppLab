package net.sunzc.demo.listener;

/**
 * Created by 振朝 on 2015/8/21.
 */
public interface OnFragmentInteractionListener {
    void onFragmentInteraction(int label);
}
