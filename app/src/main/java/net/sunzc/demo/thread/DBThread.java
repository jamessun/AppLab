package net.sunzc.demo.thread;

import android.os.HandlerThread;

/**
 * Created by 振朝 on 2015/9/20.
 */
public class DBThread extends HandlerThread {
    private static DBThread readThread, writeThread;

    private DBThread(String name) {
        super(name);
    }

    public static DBThread getReadThreadInstance() {
        if (readThread == null) {
            readThread = new DBThread("read");
        }
        return readThread;
    }

    public static DBThread getWriteThreadInstance() {
        if (writeThread == null) {
            writeThread = new DBThread("write");
        }
        return writeThread;
    }

}
