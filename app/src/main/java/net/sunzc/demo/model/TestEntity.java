package net.sunzc.demo.model;

import com.ta.util.db.annotation.TAPrimaryKey;

import java.io.Serializable;

/**
 * Created by 振朝 on 2015/9/18.
 */
public class TestEntity implements Serializable {
    private static final long serialVersionUID = -3162173956509136012L;
    @TAPrimaryKey(autoIncrement = true)
    private int id;
    private int a;
    private int b;
    private int sum;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TestEntity() {
    }

    public TestEntity(int textA, int textB, int textSum) {
        a = textA;
        b = textB;
        sum = textSum;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "A值:" + a + "--B值:" + b + "--和Sum为:" + sum;
    }
}
